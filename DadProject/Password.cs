﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DadProject
{
    public partial class Password : Form
    {
        private int count_try = 3;
        private bool accept = false;

        private string connection;

        public Password()
        {
            InitializeComponent();
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ( char.IsDigit(e.KeyChar) || e.KeyChar == (char)Keys.Back) { return; }

            e.Handled = true;
        }

        private void OK_Click(object sender, EventArgs e)
        {
            try
            {
                connection = string.Format("Server=tcp:database-denys-skrypnyk.database.windows.net,1433;Database=hyips;User ID=xakep@database-denys-skrypnyk;" +
                                           "Password={0};Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;", text_password.Text);
                if ( !GetConnectionStringFromExeConfig("connection").Equals(connection) )
                {
                    throw new Exception();
                }
                else
                {
                    accept = true;
                }
            }
            catch (Exception)
            {
                count_try--;
                string show;
                if (count_try != 1)
                {
                    show = string.Format("Неправильный пароль! У вас осталось {0} попытки!", count_try);
                }
                else
                {
                    show = string.Format("Неправильный пароль! У вас осталось попытка!");
                }
                MessageBox.Show(show, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            if (count_try == 0)
            {
                this.DialogResult = DialogResult.No;
            }
            if ( accept == true )
            {
                this.DialogResult = DialogResult.Yes;
            }
        }

        private string GetConnectionStringFromExeConfig(string connectionStringNameInConfig)
        {
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionStringNameInConfig];

            if (connectionStringSettings == null)
            {
                throw new ApplicationException(string.Format("Ошибка. '{0}' подключение не найдено!", connectionStringNameInConfig));
            }
            return connectionStringSettings.ConnectionString;
        }

        private void Password_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ( accept!= true ) this.DialogResult = DialogResult.No;
        }

        private void text_password_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter) { OK_Click(this, new EventArgs()); }
        }

        private void Password_Load(object sender, EventArgs e)
        {

        }
    }
}
