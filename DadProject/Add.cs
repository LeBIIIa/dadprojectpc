﻿using System;
using System.Windows.Forms;
namespace DadProject
{
    public partial class Add : Form
    {
        public Hyip hyip;

        public Add()
        {
            InitializeComponent();
        }

        private void Add_Load(object sender, EventArgs e)
        {
            dateDeposit.MaxDate = DateTime.Today;
            dateDeposit.Value = DateTime.Today;
            site.Text = "http://";
        }

        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;

            if (char.IsControl(e.KeyChar))
            {
                if (e.KeyChar == (char)Keys.Enter){ isControl(box.Name); }
                if (e.KeyChar == (char)Keys.Back) { return; }
            }
            switch (box.Name)
            {
                case "deposit":
                case "percentPlan":
                case "ageDeposit":
                    if (char.IsDigit(e.KeyChar)) { return; }
                    if (e.KeyChar == '.') { e.KeyChar = ','; }
                    if (e.KeyChar == ',' && box.TextLength != 0 )
                    {
                        if (box.Text.IndexOf(',') != -1){ e.Handled = true; }
                        return;
                    }
                    break;
            }
            e.Handled = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string mNameHyip, mSysPayment, mSite;
            double mDeposit, mPercentPlan;
            DateTime mDateDeposit;
            long mCountPay, mAgeDeposit;

            string mLogin, mPassword;


            try
            {
                mNameHyip = nameHyip.Text;
                mDeposit = Convert.ToDouble(deposit.Text);

                if ( sysPayment.SelectedIndex == -1 ) { throw new ArgumentNullException(); }
                mSysPayment = sysPayment.SelectedItem.ToString();
                
                mDateDeposit = dateDeposit.Value.Date;
                mPercentPlan = Convert.ToDouble(percentPlan.Text);
                if ( mNameHyip == null || mDeposit == 0 || mSysPayment == null || mDateDeposit == null || mPercentPlan == 0 )
                {
                    throw new ArgumentNullException();
                }

                mSite = site.Text;
                if ( mSite.Equals("http://") )
                {
                    throw new Exception("Укажите ссылку!");
                }

                mCountPay = 0;
                if ( per30.Checked )     { mCountPay = 30; }
                else if ( perWeek.Checked ) { mCountPay = 7; }
                else if ( perDay.Checked )  { mCountPay = 1; }
                else if ( perMonth.Checked ) { mCountPay = 31; }
                else { throw new ArgumentNullException();  }

                mAgeDeposit = Convert.ToUInt32(ageDeposit.Text);

                mLogin = login.Text;
                mPassword = password.Text;
                if ( mLogin == null || mPassword == null )
                {
                    throw new ArgumentNullException();
                }

                hyip = new Hyip(mNameHyip,mSysPayment,mSite,mDeposit,
                    mPercentPlan,mDateDeposit, mCountPay, mAgeDeposit,mLogin,mPassword);

                DialogResult = DialogResult.OK;
            }
            catch ( FormatException )
            {
                MessageBox.Show("Неправильно введены данные!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch ( ArgumentNullException )
            {
                MessageBox.Show("Заполните все поля!", "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Ошибка", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void isControl(string name)
        {
            switch (name)
            {
                case "nameHyip":
                    deposit.Focus();
                    deposit.DeselectAll();
                    break;
                case "deposit":
                    sysPayment.Focus();
                    sysPayment.SelectedIndex = 0;
                    break;
                case "percentPlan":
                    ageDeposit.Focus();
                    ageDeposit.DeselectAll();
                    break;
                case "login":
                    password.Focus();
                    password.DeselectAll();
                    break;
                case "password":
                    button1.Focus();
                    break;
            }
        }

        private void site_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Back )
            {
                TextBox box = (TextBox)sender;
                if (box.SelectionStart <= 7) { box.SelectionStart = 8; }
                if (box.TextLength == 7)    { e.Handled = true; }
                else { return; }
            }

            if (char.IsLetterOrDigit(e.KeyChar) || e.KeyChar == '-' || e.KeyChar == '_' || e.KeyChar == '.' || e.KeyChar == '~') { }
            else { e.Handled = true; }
        }

    }
}