﻿namespace DadProject
{
    partial class Add
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.nameHyip = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.deposit = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.sysPayment = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.percentPlan = new System.Windows.Forms.TextBox();
            this.dateDeposit = new System.Windows.Forms.DateTimePicker();
            this.label10 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.password = new System.Windows.Forms.TextBox();
            this.login = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.perDay = new System.Windows.Forms.RadioButton();
            this.perWeek = new System.Windows.Forms.RadioButton();
            this.perMonth = new System.Windows.Forms.RadioButton();
            this.ageDeposit = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.site = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.per30 = new System.Windows.Forms.RadioButton();
            this.SuspendLayout();
            // 
            // nameHyip
            // 
            this.nameHyip.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.nameHyip.Location = new System.Drawing.Point(12, 35);
            this.nameHyip.Name = "nameHyip";
            this.nameHyip.Size = new System.Drawing.Size(318, 22);
            this.nameHyip.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(255, 365);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 12;
            this.button1.Text = "Добавить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(95, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(144, 18);
            this.label1.TabIndex = 3;
            this.label1.Text = "Название проекта:";
            // 
            // deposit
            // 
            this.deposit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.deposit.Location = new System.Drawing.Point(12, 79);
            this.deposit.Name = "deposit";
            this.deposit.Size = new System.Drawing.Size(318, 22);
            this.deposit.TabIndex = 1;
            this.deposit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(95, 58);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(143, 18);
            this.label3.TabIndex = 6;
            this.label3.Text = "Сумма вклада в $:";
            // 
            // sysPayment
            // 
            this.sysPayment.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.sysPayment.FormattingEnabled = true;
            this.sysPayment.Items.AddRange(new object[] {
            "PerfectMoney",
            "OKPay",
            "AdvCash"});
            this.sysPayment.Location = new System.Drawing.Point(192, 123);
            this.sysPayment.Name = "sysPayment";
            this.sysPayment.Size = new System.Drawing.Size(116, 21);
            this.sysPayment.TabIndex = 2;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(172, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(156, 18);
            this.label4.TabIndex = 8;
            this.label4.Text = "Платежная система:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label5.Location = new System.Drawing.Point(141, 147);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(189, 18);
            this.label5.TabIndex = 9;
            this.label5.Text = "Дата создания депозита:";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label6.Location = new System.Drawing.Point(228, 208);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(48, 18);
            this.label6.TabIndex = 11;
            this.label6.Text = "План:";
            // 
            // percentPlan
            // 
            this.percentPlan.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.percentPlan.Location = new System.Drawing.Point(231, 234);
            this.percentPlan.MaxLength = 5;
            this.percentPlan.Name = "percentPlan";
            this.percentPlan.Size = new System.Drawing.Size(40, 22);
            this.percentPlan.TabIndex = 5;
            this.percentPlan.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.percentPlan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // dateDeposit
            // 
            this.dateDeposit.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateDeposit.Location = new System.Drawing.Point(212, 168);
            this.dateDeposit.MinDate = new System.DateTime(2014, 1, 1, 0, 0, 0, 0);
            this.dateDeposit.Name = "dateDeposit";
            this.dateDeposit.Size = new System.Drawing.Size(95, 20);
            this.dateDeposit.TabIndex = 3;
            this.dateDeposit.Value = new System.DateTime(2016, 1, 3, 0, 0, 0, 0);
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label10.Location = new System.Drawing.Point(277, 234);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(22, 18);
            this.label10.TabIndex = 19;
            this.label10.Text = "%";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label13.Location = new System.Drawing.Point(61, 344);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 18);
            this.label13.TabIndex = 25;
            this.label13.Text = "Пароль:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label14.Location = new System.Drawing.Point(60, 277);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(68, 18);
            this.label14.TabIndex = 24;
            this.label14.Text = "Аккаунт:";
            // 
            // password
            // 
            this.password.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.password.Location = new System.Drawing.Point(8, 365);
            this.password.Name = "password";
            this.password.PasswordChar = '*';
            this.password.Size = new System.Drawing.Size(175, 22);
            this.password.TabIndex = 11;
            // 
            // login
            // 
            this.login.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.login.Location = new System.Drawing.Point(8, 321);
            this.login.Name = "login";
            this.login.Size = new System.Drawing.Size(175, 22);
            this.login.TabIndex = 10;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label15.Location = new System.Drawing.Point(61, 300);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(54, 18);
            this.label15.TabIndex = 26;
            this.label15.Text = "Логин:";
            // 
            // perDay
            // 
            this.perDay.AutoSize = true;
            this.perDay.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.perDay.Location = new System.Drawing.Point(12, 107);
            this.perDay.Name = "perDay";
            this.perDay.Size = new System.Drawing.Size(90, 20);
            this.perDay.TabIndex = 6;
            this.perDay.TabStop = true;
            this.perDay.Text = "раз в день";
            this.perDay.UseVisualStyleBackColor = true;
            // 
            // perWeek
            // 
            this.perWeek.AutoSize = true;
            this.perWeek.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.perWeek.Location = new System.Drawing.Point(12, 133);
            this.perWeek.Name = "perWeek";
            this.perWeek.Size = new System.Drawing.Size(107, 20);
            this.perWeek.TabIndex = 7;
            this.perWeek.TabStop = true;
            this.perWeek.Text = "раз в неделю";
            this.perWeek.UseVisualStyleBackColor = true;
            // 
            // perMonth
            // 
            this.perMonth.AutoSize = true;
            this.perMonth.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.perMonth.Location = new System.Drawing.Point(12, 185);
            this.perMonth.Name = "perMonth";
            this.perMonth.Size = new System.Drawing.Size(98, 20);
            this.perMonth.TabIndex = 8;
            this.perMonth.TabStop = true;
            this.perMonth.Text = "раз в месяц";
            this.perMonth.UseVisualStyleBackColor = true;
            // 
            // ageDeposit
            // 
            this.ageDeposit.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.ageDeposit.Location = new System.Drawing.Point(243, 321);
            this.ageDeposit.Name = "ageDeposit";
            this.ageDeposit.Size = new System.Drawing.Size(85, 22);
            this.ageDeposit.TabIndex = 9;
            this.ageDeposit.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox_KeyPress);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label16.Location = new System.Drawing.Point(240, 300);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(67, 18);
            this.label16.TabIndex = 31;
            this.label16.Text = "Период:";
            // 
            // site
            // 
            this.site.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.site.Location = new System.Drawing.Point(8, 234);
            this.site.Name = "site";
            this.site.Size = new System.Drawing.Size(198, 22);
            this.site.TabIndex = 4;
            this.site.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.site_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(80, 208);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 18);
            this.label2.TabIndex = 33;
            this.label2.Text = "Сайт:";
            // 
            // per30
            // 
            this.per30.AutoSize = true;
            this.per30.Font = new System.Drawing.Font("Arial", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.per30.Location = new System.Drawing.Point(12, 159);
            this.per30.Name = "per30";
            this.per30.Size = new System.Drawing.Size(108, 20);
            this.per30.TabIndex = 34;
            this.per30.TabStop = true;
            this.per30.Text = "раз в 30 дней";
            this.per30.UseVisualStyleBackColor = true;
            // 
            // Add
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(342, 400);
            this.Controls.Add(this.per30);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.site);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.ageDeposit);
            this.Controls.Add(this.perMonth);
            this.Controls.Add(this.perWeek);
            this.Controls.Add(this.perDay);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.password);
            this.Controls.Add(this.login);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.dateDeposit);
            this.Controls.Add(this.percentPlan);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.sysPayment);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.deposit);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.nameHyip);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Add";
            this.Text = "Добавление вклада";
            this.Load += new System.EventHandler(this.Add_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox nameHyip;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox deposit;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox sysPayment;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox percentPlan;
        private System.Windows.Forms.DateTimePicker dateDeposit;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox password;
        private System.Windows.Forms.TextBox login;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.RadioButton perDay;
        private System.Windows.Forms.RadioButton perWeek;
        private System.Windows.Forms.RadioButton perMonth;
        private System.Windows.Forms.TextBox ageDeposit;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox site;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton per30;
    }
}