﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DadProject
{
    public partial class Editor : Form
    {
        private Hyip hyip;
        bool flag;
        private string[] payments;
        private long[] counts;

        public Editor( Hyip h )
        {
            InitializeComponent();

            hyip = h;
        }

        private void Editor_Load(object sender, EventArgs e)
        {
            payments = new string[3] { "PerfectMoney", "OKPay", "AdvCash" };
            counts = new long[4] { 1, 7, 30, 31 };


            dateTimePicker1.MaxDate = DateTime.Today;
            dateTimePicker1.MinDate = new DateTime(2014,1,1);
            dateTimePicker1.Value = hyip.StartDeposit;

            name.Text = hyip.NameHyip;
            text_site.Text = hyip.Site;
            percent.Text = hyip.PercentPlan.ToString();
            age.Text = hyip.AgeDeposit.ToString();
            deposit.Text = hyip.Deposit.ToString();
            login.Text = hyip.Login;
            password.Text = hyip.Password;

            if ( hyip.SysPayment.Equals("PerfectMoney") )
            {
                sys_payment.SelectedIndex = 0;
            }else if ( hyip.SysPayment.Equals("OKPay") )
            {
                sys_payment.SelectedIndex = 1;
            }else if ( hyip.SysPayment.Equals("AdvCash") )
            {
                sys_payment.SelectedIndex = 2;
            }

            if ( hyip.CountPay == 1 )
            {
                count_pay.SelectedIndex = 0;
            }else if ( hyip.CountPay == 7 )
            {
                count_pay.SelectedIndex = 1;
            }
            else if (hyip.CountPay == 30)
            {
                count_pay.SelectedIndex = 2;
            }
            else if (hyip.CountPay == 31)
            {
                count_pay.SelectedIndex = 3;
            }

        }

        private void Editor_FormClosing(object sender, FormClosingEventArgs e)
        {
            if ( flag == false ){
                DialogResult = DialogResult.Cancel;
            }
            else
            {
                DialogResult = DialogResult.OK;
            }
        }

        private void Ok_Click(object sender, EventArgs e)
        {
            try
            {
                string name = (this.name.Text == "") ? hyip.NameHyip : this.name.Text;
                string site = (text_site.Text == "") ? hyip.NameHyip : text_site.Text;
                double percent = (this.percent.Text == "") ? hyip.PercentPlan : double.Parse(this.percent.Text);
                long age = (this.age.Text == "") ? hyip.AgeDeposit : long.Parse(this.age.Text);
                double deposit = (this.deposit.Text == "") ? hyip.Deposit : double.Parse(this.deposit.Text);
                long count = 0;
                if ( count_pay.SelectedIndex != -1 )
                {
                    count = counts[count_pay.SelectedIndex];
                }
                string sys_payment = null;
                if ( this.sys_payment.SelectedIndex != -1 )
                {
                    sys_payment = payments[this.sys_payment.SelectedIndex];
                }
                string login = (this.login.Text == "") ? hyip.Login : this.login.Text;
                string password = (this.password.Text == "") ? hyip.Password : this.password.Text;

                DateTime start = dateTimePicker1.Value;

                long id = hyip.ID;

                Methods.Edit(id, name, site, percent, count, age, deposit, sys_payment, start, login, password);
                flag = true;
                this.Close();
            }
            catch (Exception ex){
                MessageBox.Show(ex.StackTrace, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void site_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)Keys.Back)
            {
                TextBox box = (TextBox)sender;
                if (box.SelectionStart <= 7) { box.SelectionStart = 8; }
                if (box.TextLength == 7) { e.Handled = true; }
                else { return; }
            }

            if (char.IsLetterOrDigit(e.KeyChar) || e.KeyChar == '-' || e.KeyChar == '_' || e.KeyChar == '.' || e.KeyChar == '~') { }
            else { e.Handled = true; }
        }
        private void textBox_KeyPress(object sender, KeyPressEventArgs e)
        {
            TextBox box = (TextBox)sender;

            if (char.IsControl(e.KeyChar))
            {
                if (e.KeyChar == (char)Keys.Back) { return; }
            }
            switch (box.Name)
            {
                case "deposit":
                case "percent":
                case "age":
                    if (char.IsDigit(e.KeyChar)) { return; }
                    if (e.KeyChar == '.') { e.KeyChar = ','; }
                    if (e.KeyChar == ',' && box.TextLength != 0)
                    {
                        if (box.Text.IndexOf(',') != -1) { e.Handled = true; }
                        return;
                    }
                    break;
            }
            e.Handled = true;
        }
    }

}
