﻿using System;
using System.Drawing;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Collections.Generic;
using BrightIdeasSoftware;
using System.Timers;
using System.Windows.Threading;
using System.Threading;

namespace DadProject
{
    public partial class Main : Form
    {
        private string connection_string;
        private List<Scam> list;
        private double profit, deposits, dBack;
        private string cur_table;


        public Main()
        {
            InitializeComponent();
            try
            {
                MaximumSize = new Size(MaximumSize.Width, Screen.PrimaryScreen.WorkingArea.Height - 100);
            }
            catch (ArgumentOutOfRangeException) { }
        }

        private void Main_Load(object sender, EventArgs e)
        {
            try_start();
            ListView_Load();
            cur_table = Methods.hyip;
            try
            { 
                connection_string = Methods.GetConnectionStringFromExeConfig("connection");
                refresh();
                test();
                resize();
            } catch (Exception ex) {
                MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.ExitThread();
            }
        }

        private void try_start() {
            Password new_ad = new Password();
            if (new_ad.ShowDialog() == DialogResult.No)
            {
                MessageBox.Show("Не удалось подключится к базе данных!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                Application.ExitThread();
            }
        }

        private void ListView_Load()
        {
            objectListView1.Columns[0].Width = 114;
            objectListView1.Columns[1].Width = 70;
            objectListView1.Columns[2].Width = 70;
            objectListView1.Columns[3].Width = 70;
            objectListView1.Columns[4].Width = 57;
            objectListView1.Columns[5].Width = 25;
            objectListView1.Columns[6].Width = 46;
            objectListView1.Columns[7].Width = 66;
            objectListView1.Columns[8].Width = 66;
            objectListView1.Columns[9].Width = 60;
            objectListView1.Columns[10].Width = 80;
            objectListView1.Columns[11].Width = 56;
            objectListView1.Columns[12].Width = 51;
            objectListView1.Columns[13].Width = 64;
            objectListView1.Columns[14].Width = 54;
            objectListView1.Columns[15].Width = 80;
            objectListView1.Columns[16].Width = 93;
            objectListView1.RowHeight = 40;
            for (int i = 0; i < objectListView1.AllColumns.Count; ++i)
            {
                int w = objectListView1.AllColumns[i].Width;
                objectListView1.AllColumns[i].MaximumWidth = w;
                objectListView1.AllColumns[i].MinimumWidth = w;
            }
            objectListView1.Scrollable = true;
            objectListView1.CellEditActivation = ObjectListView.CellEditActivateMode.DoubleClick;
            objectListView1.AddDecoration(new EditingCellBorderDecoration { UseLightbox = true });
            objectListView1.CellEditEnterChangesRows = false;
        }

        void test()
        {
            objectListView1.HyperlinkClicked += delegate (object sender, HyperlinkClickedEventArgs e)
            {
                foreach (Scam h in list)
                {
                    if (e.Url.Equals(h.NameHyip))
                    {
                        e.Url = h.Site;
                        e.Handled = false;
                    }
                }
            };
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Add new_ad = new Add();
            if (new_ad.ShowDialog() == DialogResult.OK)
            {
                try {
                    Methods.AddToTable(new_ad.hyip, connection_string);
                    refresh();

                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.ExitThread();
                }
            }
        }

        private void refresh()
        {
            if (list == null) { list = new List<Scam>(); }

            list.Clear();
            deposits = profit = dBack = 0;
            objectListView1.ClearObjects();

            using (SqlConnection conn = new SqlConnection(connection_string)) {
                try
                {
                    Console.WriteLine("Start!");
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"SELECT * FROM " + cur_table;
                    conn.Open();
                    SqlDataReader reader = cmd.ExecuteReader();
                    while (reader.Read())
                    {
                        Hyip h = new Hyip(reader);
                        bool flag;
                        if (cur_table.Equals(Methods.hyip))
                        {
                            flag = Methods.Update(h);
                            if (flag) {
                                Methods.Update_Table(h, connection_string);
                                deposits += h.Deposit;
                                profit += h.Profit;
                                if ( h.Profit == 0 )
                                {
                                    dBack += h.Return;
                                }
                                else
                                {
                                    dBack += h.Deposit;
                                }
                            }
                        }
                        else
                        {
                            flag = true;
                            deposits += h.Return;
                            deposits += h.RefBack;
                            deposits -= h.Deposit;
                            Methods.Update_Scam_Table(h, connection_string);
                        }
                        if (flag)
                        {
                            list.Add(h);
                        }
                    }
                    Methods.Set_Percent(list, deposits);
                    List<Hyip> obj = new List<Hyip>();
                    foreach (Hyip h in list)
                    {
                        obj.Add(h);
                    }
                    objectListView1.AddObjects(obj);
                    invested.Text = deposits + " $";
                    earned.Text = profit + " $";
                    back.Text = dBack + " $";
                    Console.WriteLine("Done!");
                }
                catch (Exception ex) { MessageBox.Show(ex.Message, Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error); }
                finally { conn.Close(); }
            }
        }

        private void objectListView1_CellEditStarting(object sender, CellEditEventArgs e)
        {
            TextBox box = new TextBox();
            box.Bounds = e.ListViewItem.GetSubItemBounds(e.SubItemIndex);
            box.Font = ((ObjectListView)sender).Font;
            box.KeyPress += delegate (object o, KeyPressEventArgs args)
            {
                if (args.KeyChar == (char)Keys.Enter)
                {
                    MessageBox.Show("");
                }
                if (args.KeyChar == (char)Keys.Back) { return; }
                if (char.IsDigit(args.KeyChar)) { return; }
                if (args.KeyChar == '.') { args.KeyChar = ','; }
                if (args.KeyChar == ',' && box.TextLength != 0)
                {
                    if (box.Text.IndexOf(',') != -1) { args.Handled = true; }
                    return;
                }
                args.Handled = true;
            };
            e.Control = box;
        }

        private void objectListView1_CellEditFinishing(object sender, CellEditEventArgs e)
        {
            string s = e.NewValue.ToString();
            if (s == null || s.Equals("") || s.Length == 0)
            {
                return;
            }

            int item_index = e.ListViewItem.Index;
            if (item_index == -1) { return; }

            OLVColumn column = e.Column;
            if (column.Index == 4) {
                list[item_index].Deposit = double.Parse(s);
            }
            else if (column.Index == 9) {
                list[item_index].RefBack = double.Parse(s);
            }
            else if (column.Index == 15) {
                list[item_index].Return = double.Parse(s);
            }
            else if (column.Index == 16) {
                list[item_index].Profit = double.Parse(s);
            }


            if (show.Text.Equals("Показать скамы"))
            {
                Methods.Update_Table((Hyip)list[item_index], connection_string);
                refresh();
            }
            else
            {
                Methods.Update_Scam_Table((Hyip)list[item_index], connection_string);
                refresh();
            }
            e.Cancel = true;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (show.Text.Equals("Показать скамы")) {
                refresh();
            }
            else {
                refresh();
            }
        }

        private void btn_ScamOrInfo_Click(object sender, EventArgs e)
        {
            if (objectListView1.SelectedIndex == -1)
            {
                MessageBox.Show("Выберите проект!", Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                int i = objectListView1.SelectedIndex;
                Hyip del = (Hyip)list[i];
                Button btn = (Button)sender;
                if (btn.Name.Equals("edit"))
                {
                    Editor edit = new Editor(del);
                    if (edit.ShowDialog() == DialogResult.OK)
                    {
                        refresh();
                    }
                }
                if (btn.Name.Equals("Scam"))
                {
                    Methods.AddToScam(del, connection_string);
                    refresh();
                }
                else if (btn.Name.Equals("info"))
                {
                    Show_Acc acc = new Show_Acc(del.Login, del.Password);
                    acc.Show();
                }
            }
        }

        private void objectListView1_FormatRow(object sender, FormatRowEventArgs e)
        {
            if (show.Text.Equals("Показать хайпы")) {
                Scam scam = (Scam)e.Model;
                if (scam.Profit > 0) { e.Item.BackColor = Color.LightGreen; }
                else { e.Item.BackColor = Color.LightCoral; }
            }
            else {
                Hyip hyip = (Hyip)e.Model;
                if (hyip.Str_DaysToPlus.Equals("+")) { e.Item.BackColor = Color.LightGreen; }
                else { e.Item.BackColor = Color.LightCoral; }
            }
        }

        private void ShowScamOrHyip(object sender, EventArgs e)
        {
            if (show.Text.Equals("Показать скамы"))
            {
                show.Text = "Показать хайпы";
                Scam.Enabled = false;
                Add.Enabled = false;
                info.Enabled = false;
                edit.Enabled = false;
                label21.Hide();
                earned.Hide();
                olvColumn17.IsEditable = true;
                olvColumn6.IsEditable = true;
                olvColumn19.IsEditable = true;
                cur_table = Methods.scam;
            }
            else
            {
                show.Text = "Показать скамы";
                olvColumn17.IsEditable = false;
                olvColumn6.IsEditable = false;
                olvColumn19.IsEditable = false;
                label21.Show();
                earned.Show();
                Scam.Enabled = true;
                Add.Enabled = true;
                info.Enabled = true;
                edit.Enabled = true;
                cur_table = Methods.hyip;
            }
            refresh();
        }


        public string Connection {
            get { return connection_string; }
        }
       
        private void resize()
        {
            objectListView1.BeginUpdate();
            objectListView1.Scrollable = true;
            objectListView1.Enabled = true;
            bool isHScroll = Methods.HasVerticalScrollBar(objectListView1);
            Console.WriteLine(isHScroll);
            if (isHScroll)
            {
                objectListView1.Width = 1146;
            }
            else {
                objectListView1.Width = 1126;
            }
            objectListView1.EndUpdate();
        }

        private void objectListView1_ClientSizeChanged(object sender, EventArgs e)
        {
            resize();
        }
    }
}
