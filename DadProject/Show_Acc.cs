﻿using System;
using System.Windows.Forms;

namespace DadProject
{
    public partial class Show_Acc : Form
    {
        private string login, password;

        public Show_Acc( string login, string password )
        {
            InitializeComponent();
            this.login = login;
            this.password = password;
        }

        private void Show_Acc_Load(object sender, EventArgs e)
        {
            label3.Text = login;
            label4.Text = password;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
