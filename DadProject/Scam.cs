﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;

namespace DadProject
{
    public class Scam
    {
        private long id;

        protected string mNameHyip,mSite, mSysPayment, mSAgeDeposit, mSCountPay, mStartEnd, mSNextPay;
        protected Image mISysPayment;
        protected double mProfit, mBack, mRefBack, mDeposit, mPercentPlan;
        protected long mCountPay, mAgeDeposit;
        protected DateTime mStartDeposit, mEndDeposit;

        public Scam(SqlDataReader reader)
        {
            id = (int)reader["Id"];
            mNameHyip = reader["name_hyip"].ToString();
            mSite = reader["site_hyip"].ToString();
            mPercentPlan = (double)reader["percent_plan"];
            mCountPay = (long)reader["count_pay"];
            mAgeDeposit = (long)reader["age_deposit"];
            mDeposit = (double)reader["deposit"];
            mSysPayment = reader["sys_payment"].ToString();
            mRefBack = (double)reader["refback"];
            mStartDeposit = (DateTime)reader["time_begin"];
            mEndDeposit = (DateTime)reader["time_end"];
            mBack = (double)reader["return_deposit"];
            mProfit = (double)reader["profit"];
        }

        public Scam(string mNameHyip, string mSysPayment, string mSite, double mDeposit,
                    double mPercentPlan, long mCountPay, long mAgeDeposit)
        {
            this.mNameHyip = mNameHyip;
            this.mSysPayment = mSysPayment;
            this.mSite = mSite;
            this.mDeposit = mDeposit;
            this.mPercentPlan = mPercentPlan;
            this.mCountPay = mCountPay;
            this.mAgeDeposit = mAgeDeposit;
        }
        //
        public string NameHyip
        {
            get { return mNameHyip; }
        }
        public string Site
        {
            get { return mSite; }
        }
        public double PercentPlan
        {
            get { return mPercentPlan; }
        }
        public long CountPay
        {
            get { return mCountPay; }
        }
        public string Str_CountPay
        {
            get { return mSCountPay; }
            set { mSCountPay = value; }
        }
        public long AgeDeposit
        {
            get { return mAgeDeposit; }
            set { mAgeDeposit = value; }
        }
        public string Str_AgeDeposit
        {
            get { return mSAgeDeposit; }
            set { mSAgeDeposit = value; }
        }
        public double Deposit
        {
            get { return mDeposit; }
            set { mDeposit = value; }
        }
        public string SysPayment
        {
            get { return mSysPayment; }
            set { mSysPayment = value; }
        }
        public double RefBack
        {
            get { return mRefBack; }
            set { mRefBack = value; }
        }
        public double Return
        {
            get { return mBack; }
            set { mBack = value; }
        }
        public double Profit
        {
            get { return mProfit; }
            set { mProfit = value; }
        }
        public DateTime StartDeposit
        {
            get { return mStartDeposit; }
        }
        public DateTime EndDeposit
        {
            get { return mEndDeposit; }
            set { mEndDeposit = value; }
        }
        public Image IMG_SysPayment
        {
            get { return mISysPayment; }
            set { mISysPayment = value; }
        }



        public long ID
        {
            get { return id; }
        }

    }
}
