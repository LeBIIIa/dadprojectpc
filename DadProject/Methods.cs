﻿using BrightIdeasSoftware;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace DadProject
{
    public static class Methods
    {

        public static string hyip = "dbo.Hyip";
        public static string scam = "dbo.Scam";

        public static bool Update(Hyip h)
        {
            bool flag = false;

            DateTime end;
            if (h.AgeDeposit == 0) { h.EndDeposit = DateTime.MaxValue; }
            else { h.EndDeposit = h.StartDeposit + new TimeSpan((int)h.AgeDeposit, 0, 0, 0); }

            set(h);
            setImage(h);

            if ( h.EndDeposit.CompareTo(DateTime.Today) < 0 )
            {
                end = h.EndDeposit;
                flag = true;
            }
            else { end = DateTime.Today; }

            try
            {
                TimeSpan span = end - h.StartDeposit;
                long q = 0;
                double payToday = 0;
                if ( h.CountPay != 30 && h.CountPay != 31 ) {
                    payToday = Math.Round(h.Deposit * (h.PercentPlan / 100), 2);

                } h.NextPay = h.StartDeposit;
                if ( h.CountPay == 31 ) { h.NextPay += new TimeSpan(DateTime.DaysInMonth(h.NextPay.Year, h.NextPay.Month), 0, 0, 0); }
                if (h.CountPay != 31) { h.NextPay += new TimeSpan(1, 0, 0, 0); }
                h.PayToday = 0;
                h.PayWeek = 0;
                Console.WriteLine(h.NameHyip);
                while (h.NextPay.CompareTo(end) <= 0)
                {
                    q++;
                    h.PayWeek += payToday;
                    if ( h.PayWeek >= h.PlanBack )
                    {
                        h.PayWeek -= h.PlanBack;
                    }

                    if (h.CountPay == 31)
                    {
                        h.NextPay += new TimeSpan(DateTime.DaysInMonth(h.NextPay.Year, h.NextPay.Month),0,0,0);
                    }else
                    {
                        h.NextPay = h.NextPay.AddDays(h.CountPay);
                    }
                }

                h.Str_NextPay = h.NextPay.ToString("dd MMM", CultureInfo.CreateSpecificCulture("ru"));
                

                if ( h.PayWeek.CompareTo(0.00f) == 0 )
                {
                    h.PayToday = 0;
                }
                else
                {
                    h.PayWeek = Math.Round(h.PayWeek, 2);
                    h.PayToday = payToday;
                }

                h.Return = Math.Round(q * (h.Deposit * (h.PercentPlan / 100)) + h.RefBack, 2);

                h.Profit = Math.Max(0, Math.Round(h.Return - h.Deposit, 2));


                if (h.AgeDeposit > 0) { h.DaysLeft = h.AgeDeposit - (long)span.TotalDays; }
                else { h.DaysLeft = 0; }
                if (h.Profit > 0) { h.Str_DaysToPlus = "+"; }
                else
                {
                    double per_pay = h.Deposit * (h.PercentPlan / 100);
                    double temp = h.Return + per_pay;
                    h.DaysToPlus = (long)span.TotalDays;
                    TimeSpan r = h.NextPay - DateTime.Today;
                    h.DaysToPlus = (long)r.TotalDays;
                    while (temp.CompareTo(h.Deposit) <= 0)
                    {
                        temp += per_pay;
                        h.DaysToPlus += h.CountPay;
                    }
                    h.Str_DaysToPlus = h.DaysToPlus.ToString();
                }
                if(flag == true) {
                    string conn = GetConnectionStringFromExeConfig("connection");
                    AddToScam(h,conn);
                    return false;
                }
                return true;
            }
            catch (ArgumentOutOfRangeException e) { throw e; }
        }

        private static void setImage(Hyip h)
        {
            if (h.SysPayment.Equals("PerfectMoney")) { h.IMG_SysPayment = Properties.Resources.perfectmoney; }
            else if (h.SysPayment.Equals("AdvCash")) { h.IMG_SysPayment = Properties.Resources.advcash; }
            else if (h.SysPayment.Equals("OKPay")) { h.IMG_SysPayment = Properties.Resources.okpay; }
        }
        private static void set(Hyip h)
        {
            if (h.EndDeposit == DateTime.MaxValue)
            {
                h.Str_AgeDeposit = "безсроч.";
                h.StartEnd = string.Format("{0}", h.StartDeposit.ToString("dd MMM", CultureInfo.CreateSpecificCulture("ru")));
            }
            else
            {
                h.Str_AgeDeposit = h.AgeDeposit.ToString();
                h.StartEnd = string.Format("{0}\n{1}", h.StartDeposit.ToString("dd MMM", CultureInfo.CreateSpecificCulture("ru")),
                h.EndDeposit.ToString("dd MMM", CultureInfo.CreateSpecificCulture("ru")));
            }
            if (h.CountPay == 1)
            {
                h.PlanBack = Math.Round(h.Deposit * (h.PercentPlan / 100) * 7, 2);
                h.Str_CountPay = "раз в\n день";
            }
            if (h.CountPay == 7)
            {
                h.PlanBack = Math.Round(h.Deposit * (h.PercentPlan / 100), 2);
                h.Str_CountPay = "раз в\n нед";
            }
            if (h.CountPay == 30) { h.Str_CountPay = "раз в\n 30 д"; }
            if (h.CountPay == 31) { h.Str_CountPay = "раз в\n мес"; }
        }

        //



        public static void Set_Percent(List<Scam> list, double deposits)
        {
            foreach (Hyip h in list)
            {
                h.PercentFromAll = Math.Round(h.Deposit / deposits * 100, 2);
            }
        }

        public static void Update_Table(Hyip h, string connection)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                try
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"UPDATE dbo.Hyip
                                        SET refback = @ref, days_left = @days, next_pay = @next, daysToplus = @plus, return_deposit = @return, profit = @profit, pay_today = @today, pay_week = @week
                                        Where id = @id";
                    cmd.Parameters.Add("@ref", SqlDbType.Float).Value = h.RefBack;
                    cmd.Parameters.Add("@days", SqlDbType.BigInt).Value = h.DaysLeft;
                    cmd.Parameters.Add("@next", SqlDbType.DateTime2).Value = h.NextPay;
                    cmd.Parameters.Add("@plus", SqlDbType.BigInt).Value = h.DaysToPlus;
                    cmd.Parameters.Add("@return", SqlDbType.Float).Value = h.Return;
                    cmd.Parameters.Add("@profit", SqlDbType.Float).Value = h.Profit;
                    cmd.Parameters.Add("@today", SqlDbType.Float).Value = h.PayToday;
                    cmd.Parameters.Add("@week", SqlDbType.Float).Value = h.PayWeek;
                    cmd.Parameters.AddWithValue("@id", h.ID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e) { throw e; }
            }
        }

        public static void AddToScam(Hyip hyip, string connection)
        {
            Scam scam = hyip;
            using (SqlConnection conn = new SqlConnection(connection))
            {
                try
                {
                    conn.Open();
                    //
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"DELETE FROM dbo.Hyip WHERE id = @id";
                    cmd.Parameters.AddWithValue("@id", hyip.ID);
                    cmd.ExecuteNonQuery();
                    //
                    cmd = conn.CreateCommand();
                    cmd.CommandText = @"INSERT INTO dbo.Scam(name_hyip, site_hyip, percent_plan, count_pay, 
                                        age_deposit, deposit, sys_payment, refback, time_begin, time_end, return_deposit, profit)
                                        VALUES(@name_hyip, @site_hyip, @percent_plan, @count_pay, @age_deposit, @deposit, @sys_payment, @refback,
                                        @time_begin, @time_end, @return_deposit, @profit)";

                    cmd.Parameters.Add("@name_hyip", SqlDbType.NVarChar, 50).Value = scam.NameHyip;
                    cmd.Parameters.Add("@site_hyip", SqlDbType.NVarChar, 50).Value = scam.Site;
                    cmd.Parameters.Add("@percent_plan", SqlDbType.Float).Value = scam.PercentPlan;
                    cmd.Parameters.Add("@count_pay", SqlDbType.BigInt).Value = scam.CountPay;
                    cmd.Parameters.Add("@age_deposit", SqlDbType.BigInt).Value = scam.AgeDeposit;
                    cmd.Parameters.Add("@deposit", SqlDbType.Float).Value = scam.Deposit;
                    cmd.Parameters.Add("@sys_payment", SqlDbType.NVarChar, 50).Value = scam.SysPayment;
                    cmd.Parameters.Add("@refback", SqlDbType.Float).Value = scam.RefBack;
                    cmd.Parameters.Add("@time_begin", SqlDbType.DateTime2).Value = scam.StartDeposit;
                    cmd.Parameters.Add("@time_end", SqlDbType.DateTime2).Value = scam.EndDeposit;
                    cmd.Parameters.Add("@return_deposit", SqlDbType.Float).Value = scam.Return;
                    cmd.Parameters.Add("@profit", SqlDbType.Float).Value = scam.Profit;
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e) { throw e; }
            }
            
        }

        public static void Update_Scam_Table(Scam h, string connection)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                try
                {
                    var cmd = conn.CreateCommand();
                    cmd.CommandText = @"UPDATE dbo.Scam SET deposit = @deposit, refback = @ref, return_deposit = @return_deposit, profit = @profit
                                        Where id = @id";
                    cmd.Parameters.Add("@deposit", SqlDbType.Float).Value = h.Deposit;
                    cmd.Parameters.Add("@ref", SqlDbType.Float).Value = h.RefBack;
                    cmd.Parameters.Add("@return_deposit", SqlDbType.Float).Value = h.Return;
                    cmd.Parameters.Add("@profit", SqlDbType.Float).Value = h.Profit;
                    cmd.Parameters.AddWithValue("@id", h.ID);
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e) { throw e; }
            }

        }

        public static void AddToTable(Hyip h, string connection)
        {
            using (SqlConnection conn = new SqlConnection(connection))
            {
                try
                {
                    SqlCommand cmd = conn.CreateCommand();
                    cmd.CommandText = @"INSERT INTO dbo.Hyip(name_hyip, site_hyip, percent_plan, count_pay, age_deposit, deposit, sys_payment, 
                        pay_today, pay_week, refback, plan_back, time_begin, time_end, days_left, next_pay, daysToplus, return_deposit, profit, login, password)
                        VALUES(@name_hyip, @site_hyip, @percent_plan, @count_pay, @age_deposit, @deposit, @sys_payment, @today, @week, @refback, @planback,
                        @time_begin, @time_end, @days_left, @next_pay, @daysToplus, @return_deposit, @profit, @login, @password)";
                    cmd.Parameters.Add("@name_hyip", SqlDbType.NVarChar, 50).Value = h.NameHyip;
                    cmd.Parameters.Add("@site_hyip", SqlDbType.NVarChar, 50).Value = h.Site;
                    cmd.Parameters.Add("@percent_plan", SqlDbType.Float).Value = h.PercentPlan;
                    cmd.Parameters.Add("@count_pay", SqlDbType.BigInt).Value = h.CountPay;
                    cmd.Parameters.Add("@age_deposit", SqlDbType.BigInt).Value = h.AgeDeposit;
                    cmd.Parameters.Add("@deposit", SqlDbType.Float).Value = h.Deposit;
                    cmd.Parameters.Add("@sys_payment", SqlDbType.NVarChar, 50).Value = h.SysPayment;
                    cmd.Parameters.Add("@today", SqlDbType.Float).Value = h.PayToday;
                    cmd.Parameters.Add("@week", SqlDbType.Float).Value = h.PayWeek;
                    cmd.Parameters.Add("@refback", SqlDbType.Float).Value = h.RefBack;
                    cmd.Parameters.Add("@planback", SqlDbType.Float).Value = h.PlanBack;
                    cmd.Parameters.Add("@time_begin", SqlDbType.DateTime2).Value = h.StartDeposit;
                    cmd.Parameters.Add("@time_end", SqlDbType.DateTime2).Value = h.EndDeposit;
                    cmd.Parameters.Add("@days_left", SqlDbType.BigInt).Value = h.DaysLeft;
                    cmd.Parameters.Add("@next_pay", SqlDbType.DateTime2).Value = h.NextPay;
                    cmd.Parameters.Add("@daysToplus", SqlDbType.BigInt).Value = h.DaysToPlus;
                    cmd.Parameters.Add("@return_deposit", SqlDbType.Float).Value = h.Return;
                    cmd.Parameters.Add("@profit", SqlDbType.Float).Value = h.Profit;
                    cmd.Parameters.Add("@login", SqlDbType.NVarChar, 50).Value = h.Login;
                    cmd.Parameters.Add("@password", SqlDbType.NVarChar, 50).Value = h.Password;
                    conn.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e) { throw e; }
            }
        }

        public static void Edit( long id, string name, string site, double percent, long count_pay, long age_deposit, double deposit, string sys_payment, DateTime start, string login, string password )
        {
            string connection = GetConnectionStringFromExeConfig("connection");
            using ( SqlConnection sql = new SqlConnection(connection) )
            {
                try
                {
                    SqlCommand cmd = sql.CreateCommand();
                    cmd.CommandText = @"UPDATE dbo.Hyip SET name_hyip = @name_hyip, site_hyip = @site_hyip, percent_plan = @percent_plan, count_pay = @count_pay,
                        age_deposit = @age_deposit, deposit = @deposit, sys_payment = @sys_payment, time_begin = @start, login = @login, password = @password
                        WHERE id = @id";
                    cmd.Parameters.Add("@name_hyip", SqlDbType.NVarChar, 50).Value = name;
                    cmd.Parameters.Add("@site_hyip", SqlDbType.NVarChar, 50).Value = site;
                    cmd.Parameters.Add("@percent_plan", SqlDbType.Float).Value = percent;
                    cmd.Parameters.Add("@count_pay", SqlDbType.BigInt).Value = count_pay;
                    cmd.Parameters.Add("@age_deposit", SqlDbType.BigInt).Value = age_deposit;
                    cmd.Parameters.Add("@deposit", SqlDbType.Float).Value = deposit;
                    cmd.Parameters.Add("@sys_payment", SqlDbType.NVarChar, 50).Value = sys_payment;
                    cmd.Parameters.Add("@start", SqlDbType.DateTime2).Value = start;
                    cmd.Parameters.Add("@login", SqlDbType.NVarChar, 50).Value = login;
                    cmd.Parameters.Add("@password", SqlDbType.NVarChar, 50).Value = password;
                    cmd.Parameters.AddWithValue("@id",id);
                    sql.Open();
                    cmd.ExecuteNonQuery();
                }
                catch (Exception e) { System.Windows.Forms.MessageBox.Show(e.Message); }
            }
        }

        public static string GetConnectionStringFromExeConfig(string connectionStringNameInConfig)
        {
            ConnectionStringSettings connectionStringSettings = ConfigurationManager.ConnectionStrings[connectionStringNameInConfig];

            if (connectionStringSettings == null)
            {
                throw new ApplicationException(string.Format("Ошибка. '{0}' подключение не найдено!", connectionStringNameInConfig));
            }
            return connectionStringSettings.ConnectionString;
        }

        //
        static public bool HasVerticalScrollBar(ListView lv)
        {
            const int GWL_STYLE = -16;
            const int WS_HSCROLL = 0x00100000;
            const int WS_VSCROLL = 0x00200000;

            int wndStyle = GetWindowLong(lv.Handle, GWL_STYLE);

            bool HorizontalVisible = (wndStyle & WS_HSCROLL) != 0;
            bool VerticalVisible = (wndStyle & WS_VSCROLL) != 0;
            if ( HorizontalVisible || VerticalVisible )
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }

        public static int GetWindowLong(IntPtr hWnd, int nIndex)
        {
            if (IntPtr.Size == 4)
                return (int)GetWindowLong32(hWnd, nIndex);
            else
                return (int)(long)GetWindowLongPtr64(hWnd, nIndex);
        }
        [DllImport("user32.dll", EntryPoint = "GetWindowLong", CharSet = CharSet.Auto)]
        public static extern IntPtr GetWindowLong32(IntPtr hWnd, int nIndex);

        [DllImport("user32.dll", EntryPoint = "GetWindowLongPtr", CharSet = CharSet.Auto)]
        public static extern IntPtr GetWindowLongPtr64(IntPtr hWnd, int nIndex);
        //
        public delegate void resize(ObjectListView obj);
    }
}
