﻿using System;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;

namespace DadProject
{
    public class Hyip : Scam
    {
        ////Hyip
        private double mPercentFromAll;

        private DateTime mNextPay;
        //
        ////Account
        private string mLogin, mPassword;
        //

        //
        private string mSDaysToPlus;
        private double mPayToday, mPayWeek, mPlanBack;
        private long mDaysLeft, mDaysToPlus;
        //

        public Hyip(string mNameHyip, string mSysPayment, string mSite, double mDeposit,
                    double mPercentPlan, DateTime mDateDeposit, long mCountPay,
                    long mAgeDeposit, string mLogin, string mPassword) : base(mNameHyip, 
                    mSysPayment, mSite, mDeposit, mPercentPlan, mCountPay, mAgeDeposit)
        {
            this.mNameHyip = mNameHyip;
            this.mSysPayment = mSysPayment;
            this.mSite = mSite;
            this.mDeposit = mDeposit;
            this.mPercentPlan = mPercentPlan;
            mStartDeposit = mDateDeposit;
            this.mCountPay = mCountPay;
            this.mAgeDeposit = mAgeDeposit;
            this.mLogin = mLogin;
            this.mPassword = mPassword;

            FillAll();
        }

        public Hyip(SqlDataReader reader) : base(reader)
        {
            if (reader.FieldCount > 14)
            {
                mPayToday = (double)reader["pay_today"];
                mPayWeek = (double)reader["pay_week"];
                mPlanBack = (double)reader["plan_back"];
                mDaysLeft = (long)reader["days_left"];
                mNextPay = (DateTime)reader["next_pay"];
                mDaysToPlus = (long)reader["daysToplus"];
                mLogin = reader["login"].ToString();
                mPassword = reader["password"].ToString();
            }
        }

        private void FillAll()
        {
            if (mAgeDeposit == 0) { mEndDeposit = DateTime.MaxValue; }
            else { mEndDeposit = mStartDeposit + new TimeSpan((int)mAgeDeposit,0,0,0); }

            mNextPay = mStartDeposit;

            if (mCountPay == 1) { mPlanBack = Math.Round(mDeposit * (mPercentPlan / 100) * 7, 2); }
            else if (mCountPay == 7) { mPlanBack = Math.Round(mDeposit * (mPercentPlan / 100), 2); }
            else if (mCountPay == 30 || mCountPay == 31 ) { mPayWeek = mPayToday = mPlanBack = 0; }

            Methods.Update(this);
        }
        
        public double PayToday
        {
            get { return mPayToday; }
            set { mPayToday = value; }
        }
        public string Str_PayToday
        {
            get
            {
                if ( PlanBack == 0) { return ""; }
                return mPayToday.ToString();
            }
        }
        //
        public double PayWeek
        {
            get { return mPayWeek; }
            set { mPayWeek = value; }
        }
        public string Str_PayWeek
        {
            get
            {
                if (PlanBack == 0) { return ""; }
                return mPayWeek.ToString();
            }
        }
        //
        public double PlanBack
        {
            get { return mPlanBack; }
            set { mPlanBack = value; }
        }
        public string Str_PlanBack
        {
            get
            {
                if ( PlanBack == 0 ) return "";
                return PlanBack.ToString();
            }
        }
        //
        public string StartEnd
        {
            get { return mStartEnd; }
            set { mStartEnd = value; }
        }
        //
        public long DaysLeft
        {
            get { return mDaysLeft; }
            set { mDaysLeft = value; }
        }
        public string Str_DaysLeft
        {
            get {
                if (mDaysLeft == -1) { return "-"; }
                else if (mDaysLeft == 0) { return ""; }
                return mDaysLeft.ToString();
            }
        }
        //
        public DateTime NextPay
        {
            get { return mNextPay; }
            set { mNextPay = value; }
        }
        public string Str_NextPay
        {
            get { return mSNextPay; }
            set { mSNextPay = value; }
        }
        //
        public long DaysToPlus
        {
            get { return mDaysToPlus; }
            set { mDaysToPlus = value; }
        }
        public string Str_DaysToPlus
        {
            get { return mSDaysToPlus; }
            set { mSDaysToPlus = value; }
        }
        //
        public double PercentFromAll
        {
            get { return mPercentFromAll; }
            set { mPercentFromAll = value; }
        }
        //
        public string Login
        {
            get { return mLogin; }
        }
        public string Password
        {
            get { return mPassword; }
        }
    }
}
